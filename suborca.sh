#!/usr/bin/env sh

# ORCA 6 submission script for Quantum Chemistry 2 HPC course.
# WS24/25 

# Setup
# -----
# 1. Copy the ORCA binaries to a directory that is accessible for the users (e.g. `/home/kurse/kurs00085/_materials`)
# 2. Change the permissions to the ORCA binaries (`chmod -R :kurs00085 orca_6_0_1_..`)

# To submit a calculation, you need to be in the calculation directory and call `suborca <inputfile>.inp`.
# The input file needs to have the following three lines:
#   `%pal nprocs X end`                 To set the number of CPU cores (max. 8).
#   `%maxcore XXXX`                     To set the memory per core (in MB, max. 3800).
#   `#timelimit short|medium|long`      To set the time limit (short=30min, medium=24h, long=3d).

COURSE="kurs00085"
ORCA_BIN="/home/kurse/kurs00085/_material/orca_6_0_1_linux_x86-64_shared_openmpi416_avx2"

# ==> Print help page with `-h` flag.
if [[ $1 == "-h" ]]; then
    echo "\nCustom ORCA 6 submission script for Lichtenberg 2 HPC"
    echo "Quantum Chemistry 2\n"
    echo "Usage: 'suborca.sh <inputfile>.inp'\n"
    echo "You need to be in the directory containing the input file you want to submit.\n"
    exit 0
fi

# ==> Parsing ORCA input file (suffix: `.inp`).
if [[ $1 == *".inp" ]]; then
    USER=$(whoami)
    JOB=$(basename $1 .inp)
    SUB_DIR=$(pwd)
    RUN_DIR="/work/scratch/kurse/$COURSE/$USER/$JOB"
    if [[ ! -f $SUB_DIR/$JOB".inp" ]]; then
        echo "Navigate into the directory of your input file to submit the calculation!"
        exit 1
    fi
else
    echo "Invalid input file. Supply '<ORCA_INPUT>.inp'."
    exit 1
fi

# ==> Parse computation parameters from input file.

# Required format: `%pal nprocs X end`
CPU_CORES=$(grep -i "%pal" $1 | awk '{print $3}')
# For this course we use a maximum of 8 CPU cores.
if (( $CPU_CORES > 12 )); then
    echo "Requested CPU cores ('$CPU_CORES') exceed the maximum value of 12."
    echo "Please set '%pal nprocs' to a maximum of 12 in your input file."
    exit 1
fi

# Required format: `%maxcore XXXX`
MEM_PER_CORE=$(grep -i "%maxcore" $1 | awk '{print $2}')

# Required format: `#timelimit <short, medium, long>`
TIMELIMIT_KEYWORD=$(grep -i "timelimit" $1 | awk '{print $2}')
case $TIMELIMIT_KEYWORD in
    short )
        # Thirty minutes.
        TIMELIMIT="00:30:00"
        ;;
    medium )
        # 24 hours.
        TIMELIMIT="24:00:00"
        ;;
    long )
        # Three days.
        TIMELIMIT="72:00:00"
        ;;
    * )
        echo "Couldn't parse the time limit '$TIMELIMIT_KEYWORD'."
        echo "Please set '#timelimit' to either 'short' (30min), 'medium' (24h), or 'long' (3d)."
        exit 1
        ;;
esac

# ==> Prepare the $RUN_DIR in the scratch directory.
if [[ -d "$RUN_DIR" ]]; then
    echo "Scratch directory $RUN_DIR already exists."
    echo "Do you want to (R)eplace the existing directory or (C)ancel and rename your job/input file ($JOB)?"
    read answer
    case $answer in
        r|R )
            # Replace the existing directory.
            rm -r "$RUN_DIR"
        ;;
        c|C )
            # Cancel (and have the user manually rename their input file).
            echo "Exiting with no changes."
            exit 0
        ;;
        * )
            echo "Couldn't read input '$answer'. Exiting with no changes."
            exit 1
        ;;
    esac
fi

# Create $RUN_DIR.
mkdir -p $RUN_DIR
echo "Created '$RUN_DIR' to run the calculation. "

# Copy relevant files to $RUN_DIR.
# Relevant files are: 1. the input file, 2. (optional) the xyz file.
cp $1 $RUN_DIR
if [ -f $SUB_DIR/*".xyz" ]; then
    cp $SUB_DIR/*".xyz" $RUN_DIR
fi

# ==> Create the SLURM input file.
echo "#!/usr/bin/env bash" >> $RUN_DIR/${JOB}.slurm

SBATCH_SETTINGS_GENERAL="
# General SLURM settings.
#SBATCH -A $COURSE
#SBATCH -p $COURSE
#SBATCH --reservation=$COURSE"
echo "$SBATCH_SETTINGS_GENERAL" >> $RUN_DIR/${JOB}.slurm

SBATCH_SETTINGS_TECHNICAL="
# Technical SLURM settings.
#SBATCH -o $JOB.out
#SBATCH -e $JOB.err
#SBATCH -t $TIMELIMIT
#SBATCH -n $CPU_CORES
#SBATCH --mem-per-cpu=$MEM_PER_CORE"
echo "$SBATCH_SETTINGS_TECHNICAL" >> $RUN_DIR/${JOB}.slurm

COMPUTE_ENV="
# Preparing the computation environment.
ml cuda/11.6
ml gcc/10.2.0
ml openmpi/4.1.6
ORCA_BIN=\"$ORCA_BIN\"
export PATH=\"\${ORCA_BIN}:\$PATH\"
export LD_LIBRARY_PATH=\"\${ORCA_BIN}:\$LD_LIBRARY_PATH\"
JOB=\"$JOB\"
SUB_DIR=\"$SUB_DIR\"
RUN_DIR=\"$RUN_DIR\""
echo "$COMPUTE_ENV" >> $RUN_DIR/${JOB}.slurm

RUN_CALCULATION="
# Running the ORCA calculation.
pushd \$RUN_DIR > /dev/null
echo \"================================\"
echo \" Quantum Chemistry 2 HPC Course\"
echo \"--------------------------------\"
echo \"SUB_DIR: \$SUB_DIR\"
echo \"RUN_DIR: \$RUN_DIR\"
echo \"DATE: \$(date)\"
echo \"USER: \$USER\"
\$ORCA_BIN/orca \${JOB}.inp
ORCA_EXITCODE=\$?
# Copy all files from \$RUN_DIR to \$SUB_DIR after the calculation is done.
cp ./* \$SUB_DIR
popd > /dev/null
exit \$ORCA_EXITCODE"
echo "$RUN_CALCULATION" >> $RUN_DIR/${JOB}.slurm

# ==> Submit job to the SLURM queueing system.
sbatch $RUN_DIR/${JOB}.slurm
